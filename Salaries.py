
# coding: utf-8

# In[67]:

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from os import path
import seaborn as sns
import math
get_ipython().magic('matplotlib inline')


# Load into the Pandas dataframe

# In[47]:

pd.read_csv('Salaries.csv')


# In[48]:

data = pd.read_csv('Salaries.csv')


# In[49]:

df = pd.DataFrame(data)
df.head()


# Description of the data

# In[25]:

# amount of data
len(df)


# In[26]:

# Variables (columns) in dataset
df.columns


# Mean of salaries

# In[27]:

np.mean(df['salary'])


# Distribution of Salary among the different persons

# In[28]:

df.salary.plot.hist(color='c')
plt.title('Salary')
plt.xlabel('dollars')


# Relation between Salary and Years of service

# In[29]:

df.plot.scatter('yrs.service', 'salary', color='orange')
plt.title('Salary/Years of service')


# Relation between Salary and Years since PhD 

# In[30]:

df.plot.scatter('yrs.since.phd', 'salary', color='green')
plt.title('Salary/Years since PhD')


# In[130]:

ax = sns.boxplot(x='rank', y='salary', hue='discipline', data = df, linewidth=2)


# In[32]:

emploee_df = df.groupby('Unnamed: 0').mean()
emploee_df.head()


# In[33]:

emploee_df.corr()


# Box plot

# one of the most important elements of statistics
# is the abbility to visualize the data on a BOX PLOT

# In[121]:

'''
on this particular BOX PLOT we can see
'''

ax = sns.boxplot(x=df["salary"])


# In[122]:

# mean of salary
np.mean(df['salary'])


# In[123]:

# maximum value of salary
np.max(df['salary'])


# In[124]:

# minimum value of salary
np.min(df['salary'])


# In[128]:

# quartiles
lst = np.sort(df.salary)

def get_median(lst):
    lst_cnt = len(lst)
    mid_idx = int(lst_cnt / 2)
    if lst % 2 !=0:
        return lst_cnt[mid_idx]
    return (lst_cnt[mid_idx-1] + lst[mid_idx]) / 2

def get_lower_half(lst):
    mid_idx = math.floor(len(lst) / 2)
    return(lst[0:mid_idx])

def get_upper_half(lst):
    mid_idx = math.ceil(len(lst) / 2)
    return(lst[mid_idx:])

def show_results(lst):
    
    q1 = np.median(get_lower_half(lst))
    q2 = np.median(lst)
    q3 = np.median(get_upper_half(lst))
    qr = q3 - q1
    
    print("Lower Half: %s" %(get_lower_half(lst)))
    print("")
    print("Upper Half: %s" %(get_upper_half(lst)))
    print("")
    print("Q1: %s" %(q1))
    print("Q2: %s" %(q2))
    print("Q3: %s" %(q3))
    print("Interquartile Range: %s" %(float(qr)))
  
show_results(lst)


# In[ ]:




# In[ ]:




# In[ ]:




# In[ ]:



